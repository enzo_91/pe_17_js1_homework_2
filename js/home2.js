let userNum = +prompt('введите число от 0 до 1000', 0);

if (userNum < 5) {
    alert('no numbers');
}

while (userNum > 1000 || !userNum) {
    userNum = +prompt('Недопустимое значение, введите пожалуйста от 0 до 1000', 0);
}

for (let i = 0; i <= userNum; i++) {
    if (i % 5 === 0) {
        alert(`число ${i}`);
    }
}